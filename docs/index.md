# Bienvenue sur le cours de terminale NSI

Ce projet a pour but de démontrer ce qui est faisable pour publier
facilement un lot de pdf.

## Comment faire

* Clonez ce dépôt dans votre futur espace gitlab
* Éditez le fichier `mkdocs.yml`
* Ajoutez du contenu
* Profitez de la vie

## Structure du projet

    mkdocs.yml                    # Le fichier de configuration
    docs/
        index.md                  # La page d'arrivée
        Chapitre1/                # Le premier chapitre de votre cours
            fichier_pdf.pdf       # Un cours au format pdf
        Chapitre2/
            00premiere_partie.pdf # Un cours en deux parties
            01deuxieme_partie.pdf # Réellement
