import os
import logging


def write_md_file(filename):
    with open(filename, "w") as f:
        logging.info(f"on écrit {filename}")
        filename = os.path.basename(filename)[:-2] + "pdf"
        f.write(
            f"""
<object data="../{filename}#pagemode=none" type="application/pdf" width="75%" height="750">
  <embed src="../{filename}#pagemode=none" type="application/pdf" width="75%" height="750"/>
</object>
"""
        )


def find_pdf_files():
    for chemin, sous_repertoires, fichiers in os.walk("."):
        for nom in fichiers:
            if nom[-3:] == "pdf":
                write_md_file(os.path.join(chemin, nom[:-3] + "md"))


if __name__ == "__main__":
    find_pdf_files()
